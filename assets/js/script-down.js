//
// Sticky Menu down js
//

/**
 * RequestAnimationFrame Polyfill
 *
 * http://paulirish.com/2011/requestanimationframe-for-smart-animating/
 * http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 * by Erik Möller, fixes from Paul Irish and Tino Zijdel
 *
 * MIT license
 */

(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}

	if ( ! window.requestAnimationFrame ) {
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); },
			timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}

	if ( ! window.cancelAnimationFrame ) {
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	}
}());


/**
 * Sticky menu script
 *
 * @link https://www.creativejuiz.fr/blog/tutoriels/creer-menu-sticky-avec-javascript-css
 */

//disable at small screen sizes
var fixed_disable_small = parseInt(tifStickyMenuInit.sticky_disable_at_width_string);

(function(w,d,undefined){
	var el_html = d.documentElement,
		el_body = d.getElementsByTagName('body')[0],
//		header = d.getElementById('masthead'),
		header = d.querySelector(tifStickyMenuInit.sticky_string),

		menuIsStuck = function(triggerElement) {
			var regexp			= /(nav\-is\-stuck)/i,
				_scrollTop		= w.pageYOffset || el_body.scrollTop,
				mybodyWidth		= parseInt(document.body.clientWidth),
				classFound		= el_html.className.match( regexp ),
				navHeight		= header.offsetHeight,
				bodyRect		= el_body.getBoundingClientRect(),
				scrollValue		= triggerElement ? triggerElement.getBoundingClientRect().top - bodyRect.top - navHeight  : 800,
				scrollValueFix	= tifStickyMenuInit.sticky_active_on_height_string >= 1 ? parseInt(tifStickyMenuInit.sticky_active_on_height_string) : scrollValue,
				scrollValFix	= classFound ? scrollValueFix : scrollValueFix + navHeight;

			// if scroll down is 700 or more AND tif-nav-is-stuck class doesn't exist
			if ( _scrollTop > scrollValFix && !classFound && mybodyWidth >= fixed_disable_small ) {
				el_html.className = el_html.className + ' tif-nav-is-stuck';
				el_body.style.paddingTop = navHeight + 'px';
			}

			// if we are to high in the page AND tif-nav-is-stuck class exists
			if ( _scrollTop <= 2 && classFound ) {
				el_html.className = el_html.className.replace( regexp, '' );
				el_body.style.paddingTop = '0px';
			}

		},

		onScrolling = function() {

			// this function fires menuIsStuck()…
			menuIsStuck( d.getElementById('main') );
			// and could do more stuff below

		};

	el_html.className = el_html.className + ' js';

	// when you scroll, fire onScrolling() function
	w.addEventListener('scroll', function(){
		w.requestAnimationFrame( onScrolling );
	});

	w.addEventListener('resize', function(){
		w.requestAnimationFrame( onScrolling );
	});

}(window, document));
