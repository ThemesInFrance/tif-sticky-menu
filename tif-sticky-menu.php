<?php
/**
 * Plugin Name: Tif - Sticky Menu
 * Plugin URI: https://themesinfrance.fr/plugins/sticky-menu/
 * Description: This plugin allows you to make your navigation “sticky” when you scroll down.
 * Version: 0.1
 * Author: Frédéric Caffin
 * Author URI: https://themesinfrance.fr
 * Text Domain: tif-sticky-menu
 * Domain Path: /inc/lang
 *
 * License: GPLv2 or later

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ( at your option ) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * -sticky-menu ( => tif-sticky-menu )
 * _sticky_menu ( => tif_sticky_menu )
 * _Sticky_Menu ( => Tif_Sticky_Menu )
 * _STICKY_MENU ( => TIF_STICKY_MENU )
 * Sticky Menu
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'TIF_STICKY_MENU_VERSION', '1.0' );
define( 'TIF_STICKY_MENU_DIR', plugin_dir_path( __FILE__ ) );
define( 'TIF_STICKY_MENU_URL', plugins_url( '/', __FILE__ ) );
define( 'TIF_STICKY_MENU_ADMIN_IMAGES_URL', plugins_url( '/assets/img/admin', __FILE__ ) ) ;

/**
 * register_uninstall_hook( $file, $callback );
 *
 * @link https://codex.wordpress.org/Function_Reference/register_uninstall_hook
 */
register_uninstall_hook( __FILE__, 'tif_sticky_menu_uninstall' );
function tif_sticky_menu_uninstall() {

	// delete_option( 'tif_plugin_mods' )['tif_plugin_sticky_menu'];
	delete_option( 'tif_plugin_sticky_menu' );

}

if ( ! defined( 'TIF_COMPONENTS_DIR' ) )
	define( 'TIF_COMPONENTS_DIR', ( get_theme_mod( 'tif_components_version' ) ? get_template_directory() . '/tif/' : plugin_dir_path( __FILE__ ) . 'tif/' ) );

// Tif compomnents
require_once TIF_COMPONENTS_DIR . 'init.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-minify.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-assets.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-posts-lists.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-thumbnail.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-mail.php';

// Sticky Menu Setup data
require_once 'inc/tif-setup-data.php';

// Sticky Menu Customizer
require_once 'inc/tif-customizer.php';

// Sticky Menu functions
require_once 'inc/tif-functions.php';

if( is_admin() ) {

	// Sticky Menu back form and register function
	require_once 'inc/admin/tif-settings-form.php';
	require_once 'inc/admin/tif-register.php';

}

/**
 * Sticky Menu Class Init
 */
class Tif_Sticky_Menu_Init {

	/**
	 * Setup class.
	**/
	function __construct() {

		// Sticky Menu Back scripts
		// add_action( 'admin_enqueue_scripts', array( $this, 'tif_sticky_menu_admin_scripts' ) );

		// Get Sticky Menu add_action
		// add_action( 'after_setup_theme', array( $this, 'tif_get_sticky_menu_callback' ) );

		// Sticky Menu Front scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'tif_sticky_menu_public_scripts' ), 110 );

		// Sticky Menu Translation
		add_action( 'init', array( $this, 'tif_sticky_menu_translation' ) );
		// add_action( 'admin_init', array( $this, 'tif_sticky_menu_polylang' ) );

		// ... Sticky Menu Assets Generation .....................................
		// Add Sticky Menu CSS to generated main CSS if configured
		add_filter( 'tif_main_css', array( $this, 'tif_get_compiled_sticky_menu_css' ), 30 );

		// Add Sticky Menu JS to generated main JS if configured
		add_filter( 'tif_main_js', array( $this, 'tif_get_sticky_menu_js' ), 30 );

		// Build standalone Sticky Menu CSS
		add_action( 'after_setup_theme', array( $this, 'tif_generate_css_from_fo' ), 20 );
		add_action( 'customize_save_after', array( $this, 'tif_create_sticky_menu_css' ), 20, false );

		// Inline Sticky Menu CSS if required
		add_action( 'wp_head', array( $this, 'tif_sticky_menu_inline_css' ), 20 );

		if( is_admin() ) {

			// Display admin notices
			add_action( 'admin_notices', array( $this, 'tif_sticky_menu_admin_notice' ), 10 );

			// Sticky Menu Menu
			add_action( 'admin_menu', array( $this, 'tif_sticky_menu_menu' ), 100 );

			// Sticky Menu Capability
			add_filter( 'option_page_capability_tif-sticky-menu', array( $this, 'tif_sticky_menu_option_page_capability' ) );

			if ( ! is_customize_preview() )
				// Sticky Menu Register
				add_action( 'admin_init', array( $this, 'tif_sticky_menu_register' ) );

		}

	}

	/**
	 * Display admin notices
	 */
	public function tif_sticky_menu_admin_notice() {

		$settings_errors = get_settings_errors( 'sticky_menu_settings_error', false );

		// DEBUG:
		// tif_print_r($settings_errors);

		// Get the current screen
		$screen = get_current_screen();

		// Return if not plugin settings page
		if ( $screen->id !== 'ajustements_page_tif-sticky-menu-options') return;

		// Checks if settings updated
		if ( isset( $settings_errors[0]['type'] ) && $settings_errors[0]['type'] == 'updated' ) {

			$this->tif_generate_css_from_bo();
			echo '<div class="notice notice-success"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		} elseif ( isset ( $settings_errors[0]['type'] )) {

			echo '<div class="notice notice-warning"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		}

	}

	/**
	 * Get Featured Pages add_action
	 */
	// static function tif_get_sticky_menu_callback() {
	//
	// 	add_action( 'tif.footer.after', 'tif_render_sticky_menu', 50 );
	//
	// }

	/**
	 * Get Sticky Menu generated assets path
	 */
	static function tif_plugin_assets_dir() {

		$tif_dir = array();
		$dirs = wp_upload_dir();
		$tif_dir['basedir'] = $dirs['basedir'] . '/plugins/tif-sticky-menu';
		$tif_dir['baseurl'] = $dirs['baseurl'] . '/plugins/tif-sticky-menu';

		/**
		 * @see https://developer.wordpress.org/reference/functions/wp_mkdir_p/
		 */

		$mkdir = array(
			'',
			'/assets',
			'/assets/css',
			// '/assets/css/blocks',
			'/assets/js',
			// '/assets/img',
			// '/assets/fonts'
		);

		foreach ( $mkdir as $key ) {

			if ( ! is_dir( $tif_dir['basedir'] . $key ) )
				wp_mkdir_p( $tif_dir['basedir'] . $key );

		}

		return $tif_dir;

	}

	/**
	 * Sticky Menu admin scripts
	 */
	public function tif_sticky_menu_admin_scripts( $hook ) {

		if ( 'settings_page_tif-sticky-menu-options' != $hook )
			return;

		// Sticky Menu Back CSS
		wp_register_style( 'tif-sticky-menu-admin', TIF_STICKY_MENU_URL . 'assets/css/admin/style' . tif_get_min_suffix() . '.css' );
		wp_enqueue_style( 'tif-sticky-menu-admin' );

		// Sticky Menu Back JS
		wp_register_script( 'tif-sticky-menu-admin', TIF_STICKY_MENU_URL . 'assets/js/admin/script' . tif_get_min_suffix() . '.js', $deps, time(), true );
		wp_enqueue_script( 'tif-sticky-menu-admin' );

	}

	/**
	 * Sticky Menu front scripts
	 */
	public function tif_sticky_menu_public_scripts() {

		$tif_generated = tif_get_option( 'plugin_sticky_menu', 'tif_init,generated', 'multicheck' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		$deps          = get_theme_mod( 'tif_components_version' ) ? array( 'tif-main' ) : false;

		if ( ! $deps || ( $deps && ! in_array( 'css', $tif_generated ) && ! is_customize_preview() ) ) {

			// Get Sticky Menu CSS dir
			$tif_dir	 = $this->tif_plugin_assets_dir();

			$filetocheck = $tif_dir['basedir'] . '/assets/css/style.css';
			$version	 = file_exists( $filetocheck ) ? date ( 'YdmHis', filemtime( $filetocheck ) ) : time();

			// Sticky Menu Front CSS if not added to TIF Main CSS
			wp_register_style( 'tif-sticky-menu', $tif_dir['baseurl'] . '/assets/css/style' . tif_get_min_suffix() . '.css', $deps, $version );
			wp_enqueue_style( 'tif-sticky-menu' );

		}

		$sticky_options	= tif_get_option( 'plugin_sticky_menu', 'tif_options', 'array' );
		$active_on_height = $sticky_options['active_on_height'];
		$localized		= 'tif-scripts';

		if ( is_home() ) {
			if ( null != $sticky_options['active_on_height_home']
				&& ( $sticky_options['active_on_height_home'] != 0 ) ) {
				$active_on_height = (int)$sticky_options['active_on_height_home'];
			}
		}

		if ( ! $deps || ( $deps && ! in_array( 'js', $tif_generated ) ) ) {

			$localized	= 'tif-sticky-menu' ;

			// Sticky Menu Front JS if not added to TIF Main JS
			wp_register_script( 'tif-sticky-menu', TIF_STICKY_MENU_URL . 'assets/js/script-' . (string)$sticky_options['up_down'] . tif_get_min_suffix() . '.js', $deps, time(), true );
			wp_enqueue_script( 'tif-sticky-menu' );

		}

		wp_localize_script(
			$localized,
			'tifStickyMenuInit',
			array(
				'sticky_string' => (string)$sticky_options['sticky_selector'] ,
				'sticky_active_on_height_string' => $active_on_height ,
				'sticky_disable_at_width_string' => $sticky_options['smallscreen_disabled']
			)
		);

	}

	/**
	 * Sticky Menu capability
	 */
	public function tif_sticky_menu_option_page_capability( $capability ) {

		if ( current_user_can( 'edit_posts' ) )
			return 'edit_posts';

	}

	/**
	 * Sticky Menu admin menu
	 */
	public function tif_sticky_menu_menu() {

		// @link https://www.base64-image.de/
		// @link http://b64.io/

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ) ) ;

		$unify_menus = get_option( 'tif_plugin_tweaks');
		$unify_menus = isset( $unify_menus['tif_init']['unify_menus'] ) && $unify_menus['tif_init']['unify_menus'] ? true : false ;

		if ( ! empty ( $GLOBALS['admin_page_hooks']['tif-tweaks-options'] ) && $unify_menus ) {

			/**
			 * @link https://developer.wordpress.org/reference/functions/add_submenu_page/
			 */
			$hook = add_submenu_page(
				'tif-tweaks-options',								// $parent_slug
				esc_html__( 'Sticky Menu', 'tif-sticky-menu' ),			// $page_title
				esc_html__( 'Sticky Menu', 'tif-sticky-menu' ),			// $menu_title
				$capability,										// $capability
				'tif-sticky-menu-options',							// $menu_slug
				'tif_sticky_menu_options_page',						// $function
				1													// $position
			);

		} else {

			/**
			 * @link https://developer.wordpress.org/reference/functions/add_options_page/
			 */
			$hook = add_options_page(
				esc_html__( 'Tif - Sticky Menu', 'tif-sticky-menu' ),			// $page_title
				esc_html__( 'Tif - Sticky Menu', 'tif-sticky-menu' ),			// $menu_title
				$capability,										// $capability
				'tif-sticky-menu-options',							// $menu_slug
				'tif_sticky_menu_options_page'						// $function
			);

		}

	}

	/**
	 * register_setting( $option_group, $option_name, $sanitize_callback );
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_setting
	 */
	public function tif_sticky_menu_register() {

		if( ! is_customize_preview() )
			register_setting( 'tif-sticky-menu', 'tif_plugin_sticky_menu', 'tif_sticky_menu_sanitize' );

	}

	/**
	 * Loads Sticky Menu translated strings
	 *
	 * @link https://developer.wordpress.org/reference/functions/load_plugin_textdomain/
	 */
	public function tif_sticky_menu_translation() {

		load_plugin_textdomain( 'tif-sticky-menu', false, basename( dirname( __FILE__ ) ) . '/inc/lang' );

	}

	/**
	 * Enable Sticky Menu translation with Polylang
	 *
	 * @link https://polylang.pro/doc/function-reference/
	 */
	// public function tif_sticky_menu_polylang() {
	//
	// 	if( ! function_exists( 'pll_register_string' ) )
	// 		return;
	//
	// 	pll_register_string(
	// 		esc_html__( 'My text.', 'tif-sticky-menu' ),
	// 		tif_get_option( 'plugin_sticky_menu', 'tif_options,text', 'text' ),
	// 		'Tif - ' . esc_html__( 'Plugin', 'tif-sticky-menu' )
	// 	);
	//
	// 	pll_register_string(
	// 		esc_html__( 'My textarea.', 'tif-sticky-menu' ),
	// 		esc_attr( tif_get_option( 'plugin_sticky_menu', 'tif_options,textarea', 'textarea' ) ),
	// 		'Tif - ' . esc_html__( 'Plugin', 'tif-sticky-menu' )
	// 	);
	//
	// }

	// ... Sticky Menu Assets Generation .........................................

	/**
	 * Get Sticky Menu assets added to tif main generated
	 */
	public function tif_get_generated_assets() {

		$tif_generated = tif_get_option( 'plugin_sticky_menu', 'tif_init,generated', 'multicheck' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		return $tif_generated;

	}

	/**
	 * Sticky Menu inline CSS for customize preview
	 */
	public function tif_sticky_menu_inline_css() {

		if ( ! is_customize_preview() )
			return;

		echo '<style type="text/css">';

		$this->tif_compile_sticky_menu_css( true );

		echo '</style>';

	}

	/**
	 * Get Sticky Menu compiled CSS
	 */
	public function tif_get_compiled_sticky_menu_css( $css ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(  ! class_exists ( 'Themes_In_France' )
			|| ! in_array( 'css', $tif_generated ) )
			return $css;

		$css .= $this->tif_compile_sticky_menu_css();

		return $css;

	}

	/**
	 * Add Sticky Menu CSS to tif-main.css if configured
	 */
	private function tif_compile_sticky_menu_css( $echo = false, $custom_css = false ) {

		$tif_css_enabled = tif_get_option( 'plugin_sticky_menu', 'tif_init,css_enabled', 'key' );
		$options         = tif_get_option( 'plugin_sticky_menu', 'tif_options', 'array' );
		$boxed_width     = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : $layout['boxed_width'];

		$plugin_scss_components = array();

		$variables = array(
			'$scss-components'                      => '(' . implode( ',', $plugin_scss_components ) . ')',

			'$tif-width-primary'                    => tif_get_length_value( tif_sanitize_length ( $boxed_width ) ),
			'$tif-width-wide'                       => '1400px',

			'$tif-sticky-menu-sticky-selector'      => '"' . (string)$options['sticky_selector'] . '"',
			'$tif-sticky-menu-transition-delay'     => (int)($options['transition_delay']) / 1000 . 's',
			'$tif-sticky-menu-fixed-opacity'        => (float)($options['fixed_opacity']),
			'$tif-sticky-menu-fixed-zindex'         => (int)($options['fixed_zindex']),
		);

		$css = null;

		// Compile Plugin CSS
		if ( $tif_css_enabled != 'custom' )
			$css .= tif_compile_scss(
				array(
					'origin'        => 'plugin',
					'components'    => array( '_style' ),
					'variables'     => $variables,
					'path'          => TIF_STICKY_MENU_DIR . 'assets/scss'
				)
			);

		$custom_colors = new Tif_Custom_Colors;
		// $color = $custom_colors->tif_colors();

		if ( $custom_css ) {
			// Add custom css after register backoffice form
			$custom_css = is_bool( $custom_css ) ? null : $custom_css ;
			$css .= strip_tags( $custom_css ) . "\n";

		} else {
			// Add custom css customize_save_after
			$css .= strip_tags( tif_get_option( 'plugin_sticky_menu', 'tif_init,custom_css', 'textarea' ) ) . "\n";
		}

		if ( $echo )
			echo $css;

		else
			return $css;

	}

	/**
	 * Create Plugin CSS
	 */
	public function tif_create_sticky_menu_css( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-sticky-menu' ) );

		// Get plugin CSS dir
		$tif_dir	= $this->tif_plugin_assets_dir();

		// Compile plugin CSS
		$plugin_css = $this->tif_compile_sticky_menu_css( false, $custom_css );

		// Create CSS
		tif_create_assets(
			array (
				'content'	=> $plugin_css,
				'type'		=> 'css',
				'path'		=> $tif_dir['basedir'] . '/assets/css/',
				'name'		=> 'style'
			)
		);

	}

	/**
	 * Generate Plugin css after register_setting()
	 */
	private function tif_generate_css_from_bo() {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-sticky-menu' ) );

		// Generate theme tif-main.css
		if( get_theme_mod( 'tif_components_version' ) ) {
			tif_create_theme_main_css( true );
			tif_create_theme_main_js( true );
		}

		// Generate plugin CSS
		$custom_css = null != get_option( 'tif_plugin_sticky_menu' )['tif_init']['custom_css']
					? get_option( 'tif_plugin_sticky_menu' )['tif_init']['custom_css']
					: true;
		$generated	= new Tif_Sticky_Menu_Init;
		$generated->tif_create_sticky_menu_css( strip_tags( $custom_css ) );

	}

	/**
	 * Force Plugin CSS if requested
	 */
	public function tif_generate_css_from_fo( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$tif_dir	= $this->tif_plugin_assets_dir();

		if ( ( isset( $_POST['tif_generate_css_nonce_field'] )
			&& wp_verify_nonce( $_POST['tif_generate_css_nonce_field'], 'tif_generate_css' )
			&& current_user_can( 'administrator' ) )
			)
			$this->tif_create_sticky_menu_css();

		if ( ! file_exists( $tif_dir['basedir'] . '/assets/css/style.css' )
			|| $custom_css
			)
			$this->tif_create_sticky_menu_css( $custom_css );

	}

	/**
	 * Add Sticky Menu JS to tif-main.js if configured
	 */
	public function tif_get_sticky_menu_js( $js ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(    class_exists ( 'Themes_In_France' )
			&& ! in_array( 'js', $tif_generated )
			)
			return $js;

			// Prevent a notice
		$tif_js = array();
		$tif_js_build = '';

		// Array of css files
		$tif_js[] = TIF_STICKY_MENU_DIR . 'assets/js/script.js';

		// Loop the css Array
		global $wp_filesystem;
		foreach ( $tif_js as $js_file ) {
			$tif_js_build .= $wp_filesystem->get_contents( $js_file );
		}

		// return the generated css
		$js .= $tif_js_build;

		return $js;

	}


}

return new Tif_Sticky_Menu_Init();
