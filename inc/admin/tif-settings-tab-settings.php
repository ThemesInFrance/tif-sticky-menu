<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( esc_html__( 'Plugin Enabled', 'tif-sticky-menu' ),
	array(
		'type'            => 'checkbox',
		'is_admin'        => true,
		'value'           => 1,
		'checked'         => tif_get_option( 'plugin_sticky_menu', 'tif_init,enabled', 'checkbox' ),
	),
	$tif_plugin_name . '[tif_init][enabled]'
);

if ( class_exists ( 'Themes_In_France' ) ) {
	$form->add_input( esc_html__( 'Add to generated file', 'tif-sticky-menu' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_sticky_menu', 'tif_init,generated', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_sticky_menu', 'tif_init,generated', 'multicheck' ),
			'options'		=> array(
				'css'			=> esc_html__( 'CSS', 'tif-sticky-menu' ),
				'js'			=> esc_html__( 'JS', 'tif-sticky-menu' ),
			),
		),
		$tif_plugin_name . '[tif_init][generated]'
	);
}

$form->add_input( esc_html__( 'CSS enabled', 'tif-sticky-menu' ),
	array(
		'type'            => 'radio',
		'is_admin'        => true,
		'checked'         => tif_get_option( 'plugin_sticky_menu', 'tif_init,css_enabled', 'radio' ),
		'options'         => array(
			''                => esc_html__( 'Plugin CSS (including custom css)', 'tif-sticky-menu' ),
			'custom'          => esc_html__( 'Custom CSS only', 'tif-sticky-menu' ),
		),
	),
	$tif_plugin_name . '[tif_init][css_enabled]'
);

$form->add_input( esc_html__( 'Allowed roles', 'tif-sticky-menu' ),
	array(
		'type'            => 'checkbox',
		'is_admin'        => true,
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ),
		'checked'         => tif_get_default( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ),
		'options'         => tif_get_wp_roles(),
	),
	$tif_plugin_name . '[tif_init][capabilities]'
);

$form->add_input( esc_html__( 'Custom CSS', 'tif-sticky-menu' ),
	array(
		'type'			=> 'textarea',
		'value'			=> tif_get_option( 'plugin_sticky_menu', 'tif_init,custom_css', 'textarea' ),
	),
	$tif_plugin_name . '[tif_init][custom_css]'
);
