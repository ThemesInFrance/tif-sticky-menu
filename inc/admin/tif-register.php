<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Sticky Menu Sanitize and register
 */

function tif_sticky_menu_sanitize( $input ) {

	// Verify that the nonce is valid.
	if ( ! isset( $_POST['tif_sticky_menu_nonce_field'] ) || ! wp_verify_nonce( $_POST['tif_sticky_menu_nonce_field'], 'tif_sticky_menu_action' ) ) :

		$code	 = 'nonce_unverified';
		$message = esc_html__( 'Sorry, your nonce did not verify.', 'tif-sticky-menu' );
		$type	 = 'error';

		return $input;

	endif;

	$code	 = 'settings_updated';
	$message = esc_html__( 'Congratulations, the data has been successfully recorded.' , 'tif-sticky-menu' );
	$type	 = 'updated';

	/**
	 * @link https://developer.wordpress.org/reference/functions/add_settings_error/
	 */
	add_settings_error(
		esc_attr( 'sticky_menu_settings_error' ),
		esc_attr( $code ),
		esc_html__( $message ),
		$type
	);

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-sticky-menu' ) );

	$new_input =
		! empty( get_option( 'tif_plugin_sticky_menu' ) )
		? get_option( 'tif_plugin_sticky_menu' )
		: tif_plugin_sticky_menu_setup_data() ;

	// Last Tab used
	$new_input['tif_tabs'] =
		! empty( $input['tif_tabs'] )
		? tif_sanitize_html( $input['tif_tabs'] )
		: false ;

	// Settings
	if(  current_user_can( 'manage_options' ) ) :
		$tif_init = array(
			'enabled'				=> 'checkbox',
			'generated'				=> 'multicheck',
			'css_enabled'			=> 'key',
			'capabilities'			=> 'multicheck',
			'custom_css'			=> 'css',
		);

		foreach ( $tif_init as $key => $value) {
			$new_input['tif_init'][$key] =
				! empty( $input['tif_init'][$key] )
				? call_user_func( 'tif_sanitize_' . $value, $input['tif_init'][$key] )
				: false ;
		}
	endif;

	// Options
	$tif_options = array(
		'up_down'					=> 'key',
		'sticky_selector'			=> 'string',
		'checkbox'					=> 'checkbox',
		'fixed_zindex'				=> 'absint',
		'fixed_opacity'				=> 'absint',
		'transition_delay'			=> 'absint',
		'smallscreen_disabled'		=> 'absint',
		'active_on_height_home'		=> 'absint',
	);

	foreach ( $tif_options as $key => $value) {
		$new_input['tif_options'][$key] =
			! empty( $input['tif_options'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_options'][$key] )
			: false ;
	}

	return $new_input;

}
