<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( esc_html__( 'Animation', 'tif-sticky-menu' ),
	array(
		'type'            => 'select',
		'selected'        => tif_get_option( 'plugin_sticky_menu', 'tif_options,up_down', 'select' ),
		'options'         => array(
			'up'             => esc_html__( 'Up', 'tif-sticky-menu' ),
			'down'           => esc_html__( 'Down', 'tif-sticky-menu' ),
		),
	),
	$tif_plugin_name . '[tif_options][up_down]'
);

$form->add_input( esc_html__( 'Class selector', 'tif-sticky-menu' ),
	array(
		'type'            => 'text',
		'required'        => true,
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,sticky_selector', 'text' ),
	),
	$tif_plugin_name . '[tif_options][sticky_selector]'
);

$form->add_input( esc_html__( 'Z-index', 'tif-sticky-menu' ),
	array(
		'type'            => 'number',
		'min'             => '0',
		'max'             => '999999',
		'step'            => '100',
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,fixed_zindex', 'string' ),
	),
	$tif_plugin_name . '[tif_options][fixed_zindex]'
);

$form->add_input( esc_html__( 'Opacity', 'tif-sticky-menu' ),
	array(
		'type'            => 'number',
		'min'             => '0',
		'max'             => '100',
		'step'            => '1',
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,fixed_opacity', 'string' ),
	),
	$tif_plugin_name . '[tif_options][fixed_opacity]'
);

$form->add_input( esc_html__( 'Transition delay (in ms)', 'tif-sticky-menu' ),
	array(
		'type'            => 'number',
		'min'             => '0',
		'max'             => '10000',
		'step'            => '100',
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,transition_delay', 'string' ),
	),
	$tif_plugin_name . '[tif_options][transition_delay]'
);

$form->add_input( esc_html__( 'Disabled for small screen (max width in px)', 'tif-sticky-menu' ),
	array(
		'type'            => 'number',
		'min'             => '0',
		'max'             => '10000',
		'step'            => '1',
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,smallscreen_disabled', 'string' ),
	),
	$tif_plugin_name . '[tif_options][smallscreen_disabled]'
);

$form->add_input( esc_html__( 'Active on (scroll height in px)', 'tif-sticky-menu' ),
	array(
		'type'            => 'number',
		'min'             => '0',
		'max'             => '1000',
		'step'            => '1',
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,active_on_height', 'string' ),
	),
	$tif_plugin_name . '[tif_options][active_on_height]'
);

$form->add_input( esc_html__( 'Active on Homepage (scroll height in px)', 'tif-sticky-menu' ),
	array(
		'type'            => 'number',
		'min'             => '0',
		'max'             => '1000',
		'step'            => '1',
		'value'           => tif_get_option( 'plugin_sticky_menu', 'tif_options,active_on_height_home', 'string' ),
	),
	$tif_plugin_name . '[tif_options][active_on_height_home]'
);
