<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_sticky_menu_options_page() {

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_sticky_menu', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-sticky-menu' ) );

	?>

	<div class="wrap">

		<h1 class="tif-plugin-title"><?php _e( 'Sticky Menu', 'tif-sticky-menu' ) ?></h1>
		<p><?php _e( 'This plugin allows you to make your navigation “sticky” when you scroll down.', 'tif-sticky-menu' ) ?></p>

		<form method="post" action="options.php" class="tif-form tif-plugin-metabox row tif-tabs">

			<?php

			settings_fields( 'tif-sticky-menu' );
			// do_settings_sections( 'tif-sticky-menu' );
			wp_nonce_field( 'tif_sticky_menu_action', 'tif_sticky_menu_nonce_field' );
			$tif_plugin_name = 'tif_plugin_sticky_menu';

			// Create a new instance
			$form = new Tif_Form_Builder();

			$count = 0;
			$tabs = 0;

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'class', 'tif-form tif-tabs' );
			$form->set_att( 'novalidate', false );
			$form->set_att( 'is_array', 'tif_plugin_sticky_menu' );
			$form->set_att( 'form_element', false );
			$form->set_att( 'add_submit', true );

			/**
			 * New tab for options
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_sticky_menu', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Options', 'tif-sticky-menu' ),
				// 'dashicon' => 'dashicons-admin-appearance',
				'dashicon' => 'dashicons-screenoptions',
				'first' => true,
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Options legend', 'tif-sticky-menu' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-options.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset>'
			) );

			/**
			 * New tab for administrator settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_sticky_menu', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Settings', 'tif-sticky-menu' ),
				'dashicon' => 'dashicons-admin-settings',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Settings legend', 'tif-sticky-menu' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-settings.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset>'
			) );

			/**
			 * New tab for help
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_sticky_menu', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Help', 'tif-sticky-menu' ),
				'dashicon' => 'dashicons-editor-help',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<div class="tif-help"><fieldset>'."\n".'<legend>' . esc_html__( 'Help', 'tif-sticky-menu' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-help.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset></div>'
			) );

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</div>',
			) );

			// Create the form
			$form->build_form();

			?>

		</form>

	</div><!-- .wrap -->

	<?php

	// echo wp_kses( tif_plugin_powered_by(), tif_allowed_html() );
	echo wp_kses( tif_memory_usage(), tif_allowed_html() );

}
