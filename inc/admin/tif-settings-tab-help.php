<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( esc_html__( 'Need some help ?', 'tif-sticky-menu' ),
	array(
		'type' => 'content',
		'value' => wp_kses( tif_plugin_help(), tif_allowed_html() ),
	)
);
