<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Sticky Menu setup data
 */

function tif_plugin_sticky_menu_setup_data() {

	return $tif_sticky_menu_setup_data = array(

		'tif_tabs'					=> 1,

		// Settings
		'tif_init'					=> array(
			'enabled'					=> true,
			'generated'					=> array( null ),
			'css_enabled'				=> '',
			'capabilities'				=> '',
			'custom_css'				=> '',
		),

		// Options
		'tif_options'				=> array(
			'up_down'					=> 'up',
			'sticky_selector'			=> '.main-navigation',
			'fixed_zindex'				=> 99999,
			'fixed_opacity'				=> 95,
			'transition_delay'			=> 300,
			'smallscreen_disabled'		=> 576,
			'active_on_height'			=> 320,
			'active_on_height_home'		=> 320,
		),

	);

}
