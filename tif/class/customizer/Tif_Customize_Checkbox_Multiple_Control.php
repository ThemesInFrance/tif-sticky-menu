<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_checkbox_multiple_control' ) ) {

	add_action( 'customize_register', 'tif_extend_checkbox_multiple_control' );

	function tif_extend_checkbox_multiple_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Checkbox_Multiple_Control extends WP_Customize_Control {

			/**
			 * Multiple checkbox customize control class.
			 *
			 * @since  1.0.0
			 * @access public
			 * @credit http://justintadlock.com/archives/2015/05/26/multiple-checkbox-customizer-control
			 */

			public $type = 'tif-checkbox-multiple';

			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				$values = !is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();?>

				<ul class="tif-multicheck">

					<?php

					if ( count( $this->choices ) >= 2 )
						echo '<input type="checkbox" name="checkall" class="checkall" /><i>' . esc_html__( 'Check all', 'canopee' ) . '</i><hr>';

					foreach ( $this->choices as $value => $label ) : ?>

						<li>

							<label>

								<input type="checkbox" value="<?php echo esc_attr( $value ); ?>" <?php checked( in_array( $value, $values ) ); ?> />
								<?php echo esc_html( $label ); ?>

							</label>

						</li>

					<?php endforeach; ?>

					<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $values ) ); ?>" />

				</ul>

			<?php
			}
		}

	}

}
