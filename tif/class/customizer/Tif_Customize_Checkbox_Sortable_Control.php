<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_checkbox_sortable_control' ) ) {

	add_action( 'customize_register', 'tif_extend_checkbox_sortable_control' );

	function tif_extend_checkbox_sortable_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Checkbox_Sortable_Control extends WP_Customize_Control {

			/**
			 * Control Type
			 */
			public $type = 'tif-checkbox-sortable';

			/**
			 * Render Settings
			 */
			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				/* Data */
				$values = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
				// $choices = array();

				$choices = $this->choices;

				$sortable = array();
				foreach ( $values as $key => $values ) {
					$sortable[$key] = ( strpos( $values, ':' ) == false ) ? $values . ':1' : $values ; /* activate all as default. */
				}

				/* If values exist, use it. */
				$options = array();
				if ( $sortable ){

					/* get individual item */
					foreach ( $sortable as $value ){

						/* separate item with option */
						$value = explode( ':', $value );

						/* build the array. remove options not listed on choices. */
						if ( array_key_exists( $value[0], $choices ) ){
							$options[$value[0]] = $value[1] ? '1' : '0';
							$hidden_value[] = $value[0] . ':' . $value[1];
						}
					}
				}

				/* if there's new options (not saved yet), add it in the end. */
				foreach ( $choices as $key => $value_ ) {

					/* if not exist, add it in the end. */
					if ( ! array_key_exists( $key, $options ) ){
						$options[$key] = '0'; // use zero to deactivate as default for new items.
						$hidden_value[] = $key . ':0' ;
					}
				}

				?>

				<ul class="tif-sortable tif-multicheck-sortable">

					<?php foreach ( $options as $key => $opt ) { ?>

						<li class="tif-multicheck-sortable-item">

							<label>

								<input id="<?php echo $this->id . '_' . esc_attr( $key ); ?>" name="<?php echo esc_attr( $key ); ?>" type="checkbox" value="<?php echo esc_attr( $key ); ?>" <?php checked( $opt ); ?> />
								<?php echo esc_html( $choices[$key] ); ?>

							</label>
							<i class="dashicons dashicons-sort"></i>

						</li>

					<?php } // end choices. ?>

						<input id="<?php echo $this->id ; ?>" name="<?php echo $this->id ; ?>" type="hidden" <?php $this->link(); ?> class="tif-multicheck-sortable-input-hidden" value="<?php echo esc_attr ( implode( ',', $hidden_value ) ); ?>" />

				</ul><!-- .tif-multicheck-sortable-list -->

			<?php

			}

		}

	}

}
