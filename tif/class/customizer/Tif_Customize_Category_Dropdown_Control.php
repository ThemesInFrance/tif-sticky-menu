<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
add_action( 'customize_register', 'tif_extend_category_dropdown_control' );

function tif_extend_category_dropdown_control( $wp_customize ) {

	if ( ! class_exists( 'WP_Customize_Control' ) )
		return null;

	class Tif_Customize_Category_Dropdown_Control extends WP_Customize_Control {
		/**
		* A class to create a dropdown for all categories in your wordpress site
		*/
		private $cats = false;

		public function __construct($manager, $id, $args = array(), $options = array()) {
			$this->cats = get_categories($options);

			parent::__construct( $manager, $id, $args );
		}

		/**
		 * Render the content of the category dropdown
		 *
		 * @return HTML
		 */
		public function render_content() {

			if ( ! empty( $this->cats ) ) {

				?>

				<label>

					<span class="tif-customizer-sub-title"><?php echo esc_html( $this->label ); ?></span>

					<select multiple <?php $this->link(); ?>>

					<?php

						foreach ( $this->cats as $cat ) {

							$value = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value() ;

							$selected = in_array( $cat->term_id, $value ) ? 'selected="selected"' : '' ;

							printf( '<option value="%s" %s>%s</option>', $cat->term_id, $selected, $cat->name);

						}

					?>

					</select>

				</label>

				<?php
			}

		}

	}

}
