<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if ( class_exists ( 'Tif_Init' ) )
	return;

require_once 'class/Tif_Init.php';
require_once 'class/Tif_Custom_Colors.php';
require_once 'class/Tif_Form_Builder.php';
require_once 'inc/admin/tif-functions.php';
require_once 'inc/tif-widgets-fields.php';
require_once 'inc/tif-functions.php';
require_once 'inc/tif-functions-properties.php';
require_once 'inc/tif-sanitize.php';
require_once 'inc/build/tif-build-blank.php';
require_once 'inc/customizer/tif-customizer-extend-control.php';
