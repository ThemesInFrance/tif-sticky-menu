jQuery( document ).ready( function($) {

	function tifGetColorBrightness(value){

		switch(value) {
			case "-60":
				return "lightest"
			break;
			case "-40":
				return "lighter"
			break;
			case "-20":
				return "light"
			break;
			case "0":
				return "normal"
			break;
			case "20":
				return "dark"
			break;
			case "40":
				return "darker"
			break;
			case "60":
				return "darkest"
			break;
			default:
				return "normal"
		}

	}

	// Check all checkbox
	$('input.checkall[type=checkbox]').click(function (event) {
		var checked = $(this).is(':checked');
		if (checked) {
			$(this).parent().find('input[type=checkbox]').prop('checked', true);
		} else {
			$(this).parent().find('input[type=checkbox]').prop('checked', false);
		}
	});

	$(document).ready(function () {
		$('ul.tif-sortable').sortable({
			axis: 'y',
			cursor: 'grabbing',
			update: function( e, ui ){
				$('ul.tif-sortable li input').trigger( 'change' );
			}
		});
	});

	/* === Tif_Customize_Color_Control for key array === */
	$( 'ul.tif-key-array-color input[type="radio"], ul.tif-key-array-color input[type="range"]' ).change(function() {

		color = $(this).parents( 'ul.tif-key-array-color' ).find( 'input[type="radio"]:checked' ).val();
		if ($(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-brightness-input-range"]').length > 0) {
			brightness = $(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-brightness-input-range"]').val();
		} else {
			brightness = 'normal';
		}
		if ($(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-opacity-input-range"]').length > 0) {
			opacity = $(this).parents( 'ul.tif-key-array-color' ).find('input[class="tif-opacity-input-range"]').val();
		} else {
			opacity = '1';
		}

		$(this).parents( 'ul.tif-key-array-color' ).find( 'input[type="hidden"]' ).val( color + ',' + tifGetColorBrightness(brightness) + ',' + opacity ).trigger( 'change' );

	});

	/* === Tif_Customize_Color_Control for hex array === */
	$( 'ul.tif-hex-array-color input[type="text"], ul.tif-hex-array-color input[type="number"]' ).change(function() {

		color = $(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="text"]' ).val();
		if ($(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="number"]' ).length > 0) {
			opacity = $(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="number"]' ).val();
		} else {
			opacity = '1';
		}

		$(this).parents( 'ul.tif-hex-array-color' ).find( 'input[type="hidden"]' ).val( color + ',normal,' + opacity ).trigger( 'change' );

	});

	/* === Tif_Customize_Box_Shadow_Control === */
	$( 'ul.tif-key-box-shadow input[type="radio"], ul.tif-key-box-shadow input[type="text"], ul.tif-key-box-shadow input[type="checkbox"], ul.tif-key-box-shadow input[type="range"]' ).change(function() {

		if ($(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="text"]' ).length > 0) {
			color = $(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="text"]' ).val();
		} else {
			color = $(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="radio"]:checked' ).val();
		}
		positionx = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-position-x-input-range"]').val();
		positiony = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-position-y-input-range"]').val();
		blur = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-blur-input-range"]').val();
		spread = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-spread-input-range"]').val();
		opacity = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[class="tif-opacity-input-range"]').val();
		inset = $(this).parents( 'ul.tif-key-box-shadow' ).find('input[type="checkbox"]').is(':checked');
		if (inset) {
			inset = 'inset';
		} else {
			inset = '';
		}

		$(this).parents( 'ul.tif-key-box-shadow' ).find( 'input[type="hidden"]' ).val( positionx + ',' + positiony + ',' + blur + ',' + spread + ',' + color + ',' + opacity + ',' + inset ).trigger( 'change' );

	});

	/* === Tif_Customize_Number_Multiple_Control === */
	$( 'ul.tif-multinumber input[type="number"], ul.tif-multinumber select' ).bind( 'change', function() {

		number_values = $(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="number"]' ).map(
			function() {
				return this.value;
			}
		).get().join(',');
		unity = $(this).parents( 'ul.tif-multinumber ' ).find( 'select' ).val();
		unity = (unity != undefined) ? ',' + unity : '';

		$(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="hidden"]' ).val( number_values + '' + unity ).trigger( 'change' );

	});

	/* === Tif_Customize_Sidebar_Layout_Control === */
	$( 'ul.tif-sidebar-layout-control input[type="radio"], ul.tif-sidebar-layout-control input[type="number"]' ).on( 'change', function() {
		tifChangeSidebarLayoutSettings( this );
	});

	function tifChangeSidebarLayoutSettings(value){

		var tmp = [];
		var inputs = [
			'input[type="radio"]:checked',
			'input[type="number"]',
		];

		$.each(inputs, function(index, input) {
				tmp.push(jQuery( value ).parents( 'ul.tif-sidebar-layout-control' ).find( input ).val())
		});

		jQuery( value ).parents( 'ul.tif-sidebar-layout-control' ).find( 'input[type="hidden"]' ).val( tmp.join(',') ).trigger( 'change' );

	}

	/* === Tif_Customize_Wrap_Attr_Main_Layout_Control === */
	$( 'ul.tif-wrap-loop-attr-control input[type="radio"], ul.tif-wrap-loop-attr-control input[type="number"], ul.tif-wrap-loop-attr-control select' ).on( 'change', function() {
		tifChangeLoopLayoutSettings( this );
	});

	$( 'ul.tif-wrap-loop-attr-control input[type="text"]' ).on( 'input', function() {
		tifChangeLoopLayoutSettings( this );
	});

	function tifChangeLoopLayoutSettings(value){

		var tmp = [];
		var inputs = [
			'input.layout[type="radio"]:checked',
			'input.post-per-line[type="number"]',
			'select.thumbnail-size',
			'select.thumbnail-ratio',
			'input.title[type="text"]',
			'select.title-tag',
			'input.title-class[type="text"]',
			'input.container-class[type="text"]',
			'input.post-class[type="text"]'
		];

		$.each(inputs, function(index, input) {
			if(jQuery( value ).parents( 'ul.tif-wrap-loop-attr-control' ).find( input ).val() != undefined ) {
				tmp.push(jQuery( value ).parents( 'ul.tif-wrap-loop-attr-control' ).find( input ).val().replace(/[,]/g, "%2C") )
			}
		});

		jQuery( value ).parents( 'ul.tif-wrap-loop-attr-control' ).find( 'input[type="hidden"]' ).val( tmp.join(',') ).trigger( 'change' );

	}

	const entryThumbnailDisabled = [
		"media_text",
		"text_media",
		"media_text_1_3",
		"text_media_1_3",
		"cover",
		"cover_media_text",
		"cover_text_media"
	];

	$('ul.tif-wrap-loop-attr-control input[type="radio"]').each(function() {
		var rowComponentsOrder = $(this).parents().eq(4).find("[id*='entry_order']");
		var rowEntryThumbnail = $("input[name='post_thumbnail']");
		if ( this.checked && jQuery.inArray( this.value, entryThumbnailDisabled ) !== -1 ) {
			// $( rowComponentsOrder ).find( rowEntryThumbnail ).prop('disabled', 'disabled');
			$( rowComponentsOrder ).find( rowEntryThumbnail ).prop('disabled', true);
		}
	});

	// Impose thumbnail for cover, media_text, media_text_1_3 loop layout
	$( 'ul.tif-wrap-loop-attr-control input[type="radio"]' ).change(function(){
		var rowComponentsOrder = $(this).parents().eq(4).find("[id*='entry_order']");
		var rowEntryThumbnail = $("input[name='post_thumbnail']");
		if ( this.checked && jQuery.inArray( this.value, entryThumbnailDisabled ) !== -1 ) {
			$( rowComponentsOrder ).find( rowEntryThumbnail ).prop('checked', 'checked').change();
			$( rowComponentsOrder ).find( rowEntryThumbnail ).prop('disabled', true);
		} else {
			$( rowComponentsOrder ).find( rowEntryThumbnail ).prop('disabled', false);
			// $( rowComponentsOrder ).find( rowEntryThumbnail ).removeAttr("disabled");
		}
	});

	/* === Tif_Customize_Wrap_Attr_Content_Control === */
	$( 'ul.tif-wrap-container-attr-control select' ).on( 'change', function() {
		tifChangeWrapAttrSettings( this );
	});

	$( 'ul.tif-wrap-container-attr-control textarea' ).on( 'input', function() {
		tifChangeWrapAttrSettings( this );
	});

	$( 'ul.tif-wrap-container-attr-control input[type="text"]' ).on( 'input', function() {
		tifChangeWrapAttrSettings( this );
	});

	function tifChangeWrapAttrSettings(value){

		var tmp = [];
		var inputs = [
			'textarea.content',
			'input.content[type="text"]',
			'select.container-tag',
			'input.container-class[type="text"]',
		];

		$.each(inputs, function(index, input) {
			if(jQuery( value ).parents( 'ul.tif-wrap-container-attr-control' ).find( input ).val() != undefined ) {
				tmp.push(jQuery( value ).parents( 'ul.tif-wrap-container-attr-control' ).find( input ).val().replace(/[,]/g, "%2C") )
			}
		});

		jQuery( value ).parents( 'ul.tif-wrap-container-attr-control' ).find( 'input[type="hidden"]' ).val( tmp.join(',') ).trigger( 'change' );

	}

	/* === Tif_Customize_Image_Cover_Fit_Control === */
		$( 'ul.tif-cover-fit-control input[type="radio"], ul.tif-cover-fit-control input[type="checkbox"], ul.tif-cover-fit-control input[type="number"], ul.tif-cover-fit-control select' ).on( 'change', function() {
		tifChangeCoverSettings( this );
	});

	$( 'ul.tif-cover-fit-control input[type="text"]' ).on( 'input', function() {
		tifChangeCoverSettings( this );
	});

	function tifChangeCoverSettings(value){

		var tmp = [];
		var inputs = [
			'select.cover-fit-preset',
			'input[type="radio"]:checked',
			'input[type="number"]',
			'select.cover-fit-preset-height-unit',
		];

		$.each(inputs, function(index, input) {
			if(jQuery( value ).parents( 'ul.tif-cover-fit-control' ).find( input ).val() != undefined ) {
				tmp.push(jQuery( value ).parents( 'ul.tif-cover-fit-control' ).find( input ).val().replace(/[,]/g, "%2C") )
			}
		});

		let fixed = 0;
		if (jQuery( value ).parents( 'ul.tif-cover-fit-control' ).find( 'input.cover-fixed[type="checkbox"]' ).is(':checked')) {
			fixed = 1;
		}

		jQuery( value ).parents( 'ul.tif-cover-fit-control' ).find( 'input[type="hidden"]' ).val( tmp.join(',') + ',' + fixed ).trigger( 'change' );

	}

	// Show/Hide according to the preset value
	$(".tif-cover-fit-control").each(function(){
		$(this).change(function(){
			const imageFit = $(this).parent().find("select.cover-fit-preset").val();
			if(imageFit == 'default') {
				$(this).parent().find('.customize-control-cover-fit-position').hide();
				$(this).parent().find('.customize-control-cover-fit-height').hide();
				$(this).parent().find('.customize-control-cover-fit-fixed').hide();
			} else {
				$(this).parent().find('.customize-control-cover-fit-position').show();
				$(this).parent().find('.customize-control-cover-fit-height').show();
				$(this).parent().find('.customize-control-cover-fit-fixed').show();
			}
		})
	}).change();

	/* === Tif_Customize_Checkbox_Multiple_Control === */
	$( 'ul.tif-multicheck input[type="checkbox"]' ).on( 'change', function() {

		checkbox_values = $(this).parents( 'ul.tif-multicheck' ).find( 'input:not(".checkall")[type="checkbox"]:checked' ).map(
			function() {
				return this.value;
			}
		).get().join(',');

		$(this).parents( 'ul.tif-multicheck' ).find( 'input[type="hidden"]' ).val( checkbox_values ).trigger( 'change' );

	});

	/* === Tif_Customize_Checkbox_Sortable_Control === */
	$( 'ul.tif-multicheck-sortable input[type="checkbox"]' ).on( 'change', function() {

		sortable_values = $(this).parents( 'ul.tif-multicheck-sortable' ).find( 'input[type="checkbox"]' ).map( function() {
			var active = '0';
			if( $(this).prop("checked") ){
				active = '1';
			}
			return this.value + ':' + active;
		}).get().join(',');

		$(this).parents( 'ul.tif-multicheck-sortable' ).find( 'input[type="hidden"]' ).val( sortable_values ).trigger( 'change' );

	});


	/* === Tif_Customize_Text_Sortable_Control === */
	$( 'ul.tif-multitext-sortable input[type="text"]' ).on( 'change', function() {

		sortable_values = $(this).parents( 'ul.tif-multitext-sortable' ).find( 'input[type="text"]' ).map( function() {
			var active = null;
			if( $(this).val().length != 0 ){
				active = $(this).attr('name') + ':' + encodeURIComponent($(this).val());
			}
			return active;
		}).get().join(',');

		$(this).parents( 'ul.tif-multitext-sortable' ).find( 'input[type="hidden"]' ).val( sortable_values ).trigger( 'change' );

	});

	/* === Tif_Customize_Number_Multiple_Control === */
	$( 'ul.tif-multinumber input[type="number"], ul.tif-multinumber select' ).bind( 'change', function() {

		number_values = $(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="number"]' ).map(
			function() {
				return this.value;
			}
		).get().join(',');
		unity = $(this).parents( 'ul.tif-multinumber ' ).find( 'select' ).val();
		unity = (unity != undefined) ? ',' + unity : '';

		$(this).parents( 'ul.tif-multinumber ' ).find( 'input[type="hidden"]' ).val( number_values + '' + unity ).trigger( 'change' );

	});

	/* === Tif_Customize_Select_Multiple_Control === */
	$( 'ul.tif-multiselect select' ).bind( 'change', function() {

		var values = new Array();
		var select_values;
		$.each($(this).parents( 'ul.tif-multiselect' ).find( ':selected' ), function() {
			values.push($(this).val());
		});
		select_values = values.join(',');

		$(this).parents( 'ul.tif-multiselect' ).find( 'input[type="hidden"]' ).val( select_values ).trigger( 'change' );

		});

	/* === Tif_Customize_Range_Multiple_Control === */
	$( 'ul.tif-multirange input[type="range"]' ).on( 'change', function() {

		range_values = $(this).parents( 'ul.tif-multirange' ).find( 'input[type="range"]' ).map(
			function() {
				return this.value;
			}
		).get().join(',');

		$(this).parents( 'ul.tif-multirange' ).find( 'input[type="hidden"]' ).val( range_values ).trigger( 'change' );

	});

} ); // jQuery( document ).ready

/* === Tif_Customize_Text_Editor_Control === */
jQuery( document ).ready(function($) {
	"use strict";
	$('.customize-control-tinymce-editor').each(function(){
		// Get the toolbar strings that were passed from the PHP Class
		var tinyMCEToolbar1String = _wpCustomizeSettings.controls[$(this).attr('id')].tif_tinymce_toolbar1;
		var tinyMCEToolbar2String = _wpCustomizeSettings.controls[$(this).attr('id')].tif_tinymce_toolbar2;
		var tinyMCEMediaButtons = _wpCustomizeSettings.controls[$(this).attr('id')].tif_media_buttons;

		wp.editor.initialize( $(this).attr('id'), {
			tinymce: {
				wpautop: true,
				toolbar1: tinyMCEToolbar1String,
				toolbar2: tinyMCEToolbar2String
			},
			quicktags: true,
			mediaButtons: tinyMCEMediaButtons
		});
	});
	$(document).on( 'tinymce-editor-init', function( event, editor ) {
		editor.on('change', function(e) {
			tinyMCE.triggerSave();
			$('#'+editor.id).trigger('change');
		});
	});
}); // jQuery( document ).ready
