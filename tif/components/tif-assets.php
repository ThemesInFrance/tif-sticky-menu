<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once( ABSPATH . 'wp-admin/includes/file.php' );

// Prevent PHP errors if the host provides a minimal configuration without the ctype.so extension
if ( ! function_exists( 'ctype_alpha' ) ) {
	function ctype_alpha( $text ){
		return preg_match( "/[A-Za-z]/", $text );
	}
}

// Prevent PHP errors if the host provides a minimal configuration without the ctype.so extension
if ( ! function_exists( 'ctype_space') ) {
	function ctype_space( $text ) : bool {
		return preg_match('%^\s+$%', $text) === 1;
	}
}

// Prevent PHP errors if the host provides a minimal configuration without the ctype.so extension
if ( ! function_exists( 'ctype_digit') ) {
	function ctype_digit( $text ) {
		return preg_match('#^-?\d+$#', $text) && is_int((int) $text);
	}
}

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\ValueConverter;
function tif_compile_scss( $args = array() ) {

	$default_args  = array(
		'origin'        => 'stylesheet',
		'components'    => array(),
		'variables'     => array(),
		'path'          => null,
		'output'        => array(
			'abspath'       => false,
			'path'          => null,
			'name'          => null,
		),
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	if ( empty( $parsed['components'] ) || null ==  $parsed['path'] )
		return;

	require_once 'scssphp/scss.inc.php';

	$css = null;
	foreach ( $parsed['components'] as $key ) {

		if ( $parsed['origin'] == 'abspath' || $parsed['origin'] == 'plugin' ) :
			$scssContents = file_get_contents(
				$parsed['path']. '/' . $key . '.' . 'scss'
			);
			$import_path  = $parsed['path'];

		else :
			// Origin (template or stylesheet)
			$get_assets   = 'tif_get_' . $parsed['origin'] . '_assets';
			$scssContents = file_get_contents(
				$get_assets( $parsed['path'] . '/', $key, 'scss' )
			);
			$import_path  = get_template_directory() . $parsed['path'];

		endif;

		if ( is_bool( $parsed['output']['abspath'] ) && $parsed['output']['abspath'] ) :
			$cssTarget = $parsed['output']['path'];

		else :
			$cssTarget = get_template_directory() . $parsed['output']['path'] . '/';

		endif;


		$variables = array();
		$compiler  = new Compiler();
		$compiler->addImportPath( $import_path );

		foreach ( $parsed['variables'] as $key => $value ) {

			if ( null != $value )
				$variables[$key] = ValueConverter::parseValue( $value );

			else
				$variables[$key] = ValueConverter::fromPhp( $value );

		}

		// DEBUG:
		// tif_print_r($variables);

		$compiler->replaceVariables( $variables );
		$tmp = $compiler->compileString( $scssContents )->getCss();

		if ( ! empty( $tmp ) && is_string( $tmp ) && null != $parsed['output']['path'] && null == $parsed['output']['name'] ) :
			tif_create_assets(
				array (
					'content'    => $tmp,
					'type'       => 'css',
					'path'       => $cssTarget,
					'name'       => $key
				)
			);
		else:
			$css .= $tmp;
		endif;

	}

	if ( ! empty( $css ) && is_string( $css ) && null != $parsed['output']['path'] && null != $parsed['output']['name'] ) :

		tif_create_assets(
			array (
				'content'    => $css,
				'type'       => 'css',
				'path'       => $cssTarget,
				'name'       => $parsed['output']['name']
			)
		);

	else:

		return $css;

	endif;

}

function tif_compile_scss_content( $args = array() ) {

	$default_args  = array(
		'content'      => null,
		'variables'    => array(),
		'output'       => array(
			'abspath'      => false,
			'path'         => null,
			'name'         => null,
		),
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	if ( empty( $parsed['content'] ) )
		return;

	require_once 'scssphp/scss.inc.php';

	$css       = null;
	$variables = array();
	$compiler  = new Compiler();

	foreach ( $parsed['variables'] as $key => $value ) {

		if ( null != $value )
			$variables[$key] = ValueConverter::parseValue( $value );

		else
			$variables[$key] = ValueConverter::fromPhp( $value );

	}

	// DEBUG:
	// tif_print_r($variables);

	$compiler->replaceVariables( $variables );
	$css = $compiler->compileString( $parsed['content'] )->getCss();

	if ( ! empty( $css ) && is_string( $css ) && null != $parsed['output']['path'] && null != $parsed['output']['name'] ) :

		tif_create_assets(
			array (
				'content'    => $css,
				'type'       => 'css',
				'path'       => $cssTarget,
				'name'       => $parsed['output']['name']
			)
		);

	else:

		return $css;

	endif;

}

function tif_concat_assets( $args = array() ) {

	$default_args  = array(
		'origin'        => 'stylesheet',
		'components'    => array(),
		'type'          => 'css',
		'path'          => null,
		'output'        => array(
			'path'          => null,
			'name'          => null
		),
	);

	$parsed = tif_parse_args_recursive( $args, $default_args );

	if ( empty( $parsed['components'] ) || null == $parsed['path'] )
		return;

	$parsed['components'] = ! is_array( $parsed['components'] ) ? explode( ',', $parsed['components'] ) : $parsed['components'] ;

	// Prevent a notice
	$concat = '';

	// Loop the css Array
	foreach ( $parsed['components'] as $key ) {
		if ( $parsed['origin'] == 'abspath' || $parsed['origin'] == 'plugin' ) :

			$path = null != $parsed['path'] ? $parsed['path'] . '/' : null;
			$concat .= file_get_contents(
				 $path . $key . '.' . $parsed['type']
			);
			$concatTarget = $parsed['output']['path'];

		else :

			// Origin (template or stylesheet)
			$get_assets = 'tif_get_' . $parsed['origin'] . '_assets';

			$concat .= file_get_contents(
				 $get_assets( $parsed['path'] . '/', $key, $parsed['type'] )
			);
			$concatTarget = get_template_directory() . $parsed['output']['path'];

		endif;
	}

	if ( ! empty( $concat ) && is_string( $concat ) && null != $parsed['output']['path'] && null != $parsed['output']['name'] ) :
		tif_create_assets(
			array (
				'content'    => $concat,
				'type'       => $parsed['type'],
				'path'       => $concatTarget,
				'name'       => $parsed['output']['name']
			)
		);
	else:
		return $concat;
	endif;

}

function tif_create_assets( $args = array() ) {

	$defaults  = array(
		'content'    => null,
		'type'           => 'css',
		'path'           => null,
		'name'           => null
	);

	$parsed = wp_parse_args( $args, $defaults );

	if ( empty( $parsed['content'] ) || null ==  $parsed['path'] || null ==  $parsed['name'] )
		return;

	$minimized = $parsed['type'] == 'css' ? 'tif_minimize_css' : 'tif_minimize_js' ;

	file_put_contents(
		$parsed['path'] . '/' . $parsed['name'] . '.min.' . $parsed['type'],
		$minimized( $parsed['content'] ),
	);

	file_put_contents(
		$parsed['path'] . '/' . $parsed['name'] . '.' . $parsed['type'],
		$parsed['content'],
	);

}
